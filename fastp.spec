Name:           fastp
Version:        0.24.0
Release:        1
Summary:        An ultra-fast all-in-one FASTQ preprocessor

License:        MIT
URL:            https://github.com/OpenGene/%{name}
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  make gcc-c++ zlib-devel libdeflate-devel libisal

%description
A tool designed to provide fast all-in-one preprocessing for FastQ files. This
tool is developed in C++ with multithreading supported to afford high performance.


%prep
%autosetup
# do not need bundled zlib header files
%{__rm} -rf src/zlib


%build
CXXFLAGS="%{optflags} -DDYNAMIC_ZLIB" \
%make_build


%install
install -d -p -m 0755 %{buildroot}%{_bindir}
PREFIX="%{buildroot}%{_prefix}" \
%make_install


%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}


%changelog
* Mon Dec 09 2024 liyunqing <liyunqing@kylinos.cn> - 0.24.0-1
- update version to 0.24.0.
  - fix some possible memory leaks
  - fix a crash caused by dual-delete
  - reduce pack size and pack limit to avoid possible huge memory usage
  - fix a bug of fixMGI
  - Highlight the new publication

* Mon Jan 08 2024 lichao <lichaod@uniontech.com> - 0.23.4-1
- update version.

* Fri Jan 28 2022 herengui <herengui@uniontech.com> - 0.23.2-1
- update version.

* Thu Jun 17 2021 wangqing <wanngqing@uniontech.com> - 0.20.1-1
- Package init
